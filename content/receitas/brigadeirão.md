---
title: "Brigadeirão"
date: 2021-10-14T13:50:28-03:00
draft: false
---
![brigadeirão](/comer-bem-nos-faz-bem/images/brigadeirão.jpeg)

## Ingredientes
- 2 latas de leite condensado
- 4 gemas
- 4 colheres de chocolate
- 1 colher de margarina

## Modo de preparo

Colocar tudo no liquidificador ou batedeira e bater até formar uma massa homogênea. Assar a 180° em banho maria. Separar primeiro o ovo, acrescentar o leite condensado, a margarina e bater tudo. Ao colocar no forno, a àgua do banho maria está fria. A forma é recoberta/tampada com papel alumínio.
---
title: "Biscoitinhos da dona Lalá"
date: 2021-10-14T13:50:28-03:00
draft: false
---
![biscoitinhos da dona Lalá](/comer-bem-nos-faz-bem/images/biscoitinhos_da_dona_lala.jpg)

## Ingredientes
- 2 ovos
- 2 xícaras de açúcar
- 1/2 pote de margarida = 250g
- 5 xícaras de farinha
- 1 colher chá de fermento
- 1 pacote de coco ralado

## Modo de preparo
Coloque tudo numa tigela grande e mistura com as mãos até formar uma massa. Molde a massa a sua escolha. Pingue algumas gotas de óleo numa fôrma e espalhe bem. Ponha os biscoitos na forma e leve ao forno a mais ou menos 180°C e fique atento,pois eles assam muito rápido. Coloque-os em uma bandeja e sirva-os.
---
title: "Cookies"
date: 2021-10-14T13:50:28-03:00
draft: false
---
![cookies](/comer-bem-nos-faz-bem/images/cookies.jpg)

## Ingredientes
- 3/4 de xícara de açúcar
- 3/4 de xícara de açúcar mascavo
- 12 colheres de sopa de manteiga derretida
- 1 ovo
- 2 1/4 xícaras de farinha
- 1 colher de chá de fermento
- 1 colher de chá de extrato de baunilha
- 1/2 colher de chá de fermento
- 1 xícara de chocolate em gotas

## Modo de preparo
Misturar bem o açúcar com a manteiga e o ovo, depois adicionar o resto.
Assar a 200°C por 15 a 20min
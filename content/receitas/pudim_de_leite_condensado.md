---
title: "Pudim de leite condensado"
date: 2021-10-14T13:50:28-03:00
draft: false
---
![pudim de leite condensado](/comer-bem-nos-faz-bem/images/pudim_de_leite_condensado.jpg)

## Ingredientes
- 3 ovos
- 1 lata de leite condensado
- 1 lata de leite comum
- 1 colher de café de maisena

## Modo de preparo
Caramelize uma forma. Bata os 3 ovos e depois adicione o resto dos ingredientes e bata de novo. Assar a 180° por 1 hora em banho maria.
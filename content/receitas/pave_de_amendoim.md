---
title: "Pave de amendoim"
date: 2021-10-14T13:50:28-03:00
draft: false
---
![pave de amendoim](/comer-bem-nos-faz-bem/images/pave_de_amendoim.jpg)

## Ingredientes
- 3/4kg de bolacha maisena
- 1 lata de creme de leite (gelado e sem soro)
- 1 lata de leite condensado cozido por 25min
- 200g de margarina
- 1 xícara de açúcar
- 200g de amendoim torrado, sem casca e moído

## Modo de preparo
Fazer um creme batendo o creme de leite, a margarina e o açúcar. Untar uma forma com o creme e por camadas de bolacha, creme, bolacha... Terminando em bolacha. Espalhar leite condensado por cima e terminar cobrindo com o amendoim. Levar à geladeira.
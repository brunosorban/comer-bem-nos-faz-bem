---
title: "Apfelstrudel"
date: 2021-10-14T13:50:28-03:00
draft: false
---
![Apfelstrudel](/comer-bem-nos-faz-bem/images/apfelstrudel.jpg)

## Ingredientes
- 1/2kg de farinha de trigo
- 2 colheres de sopa de óleo
- 2 colheres de vinagre
- +/- 300ml de água morna
- Sal à gosto

## Modo de preparo

Sovar bem a massa, até ela soltar da bacia e criar bolhas, massa bem mole. Deixar descansar por 30min. Esticar bem a massa numa mesa grande, com uma toalha e bastante farinha, até ficar bem fininha. Colocar o recheio, dobrar uma vez em cada canto e colocar mais recheio, depois enrolar formando um canudão. Assar numa forma untada com óleo e passar um fio de olho sobre o rolo antes de assar.

## Recheio
- "Salpicar" óleo por toda a massa
- Maçã sem casca e ralada, escolher a com menos água que tiver (espalhar bem e não acumular muito)
- Farinha de rosca (para secar a água da maçã) ou algo de mesma função (coco seco,...)
- Açúcar e canela
- Nozes ou castanha-do-pará bem miudinho
- Uva passa (opcional)
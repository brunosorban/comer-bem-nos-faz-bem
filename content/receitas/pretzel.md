---
title: "Pretzel"
date: 2021-10-14T13:50:28-03:00
draft: false
---
![pretzel](/comer-bem-nos-faz-bem/images/pretzel.jpg)

## Ingredientes
- 3 xícaras de farinha
- 1 colher de manteiga
- 3 colheres de açúcar
- 1 colher de sobremesa de fermento
- 1 pitada de sal
- Raspa de laranja ou limão
- Acrescentar leite para dar o ponto

## Modo de preparo
Misturar tudo até formar uma massa homogênea e consistente, como uma pizza. Um formato legal é cortar em tiras, fazer um rasgo no meio e dobrar de modo que uma das pontas passe pelo centro do rasgo. Fritar tudo e passar no açúcar e canela
